var moduleCode = '02002';
function initFun() {
	if (secure.find) {
		findDepartment();
		findListInfo();
		dialog = BootstrapDialog.loading();
	}
	if(!secure.add) $('button.output-salaryList-btn').remove();
	if(secure.add ) $('button.output-salaryList-btn').removeClass('hide');
}
$(function() {
	$('.search-select, .dropdown-menu').on('click', function(e) {
		$target = $(e.target);
		var searchBtn = $('button.search-btn');
		searchBtn.text($target.text());
		searchBtn.append("<span class='caret'></span>");
		searchBtn.attr('name', $target.attr('name'));
	});
	$('select.select-department').change(function(e){
		findPosition($(this).val());
	});
});
/*
 * 新窗口打开新建员薪水息页面
 * te5l.com [K]
 */
function newInternshipEmpl() {
	window.open('./employees_internship_info.html?moduleCode=02002');
}
/*
 * 获取薪水列表信息
 * te5l.com [K]
 */
function findListInfo() {
	var serType = $('button.search-btn').attr('name');
	var serVal = $('input.search-val').val();
	var department = $('select.select-department').val();
	var position = $('select.select-position').val();
	var salaryMonth = $('input.select-salary-time').val();
	if(salaryMonth==undefined){
		salaryMonth = "NoSalaryMonth";
	}
	$.post('./mgr/salary/findAllSalaryList', {
		serType : serType,
		serVal : serVal,
		department : department,
		position : position,
		page : page,
		salaryMonth : salaryMonth
	}, function(data) {
		var tbody = $('tbody.tbody-info').empty();
		dialog.close();
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i,v){
			$("<tr></tr>")
			.append($("<td></td>").append(v.employeesName))
			.append($("<td></td>").append(v.emId))
			.append($("<td></td>").append(v.sex ? '女' : '男'))
			.append($("<td></td>").append(v.deparemtn))
			.append($("<td></td>").append(v.baseSalary))
			.append($("<td></td>").append(v.lateSalary))
			.append($("<td></td>").append(v.absenceSalary))
			.append($("<td></td>").append(v.wuxianyijin))
			.append($("<td></td>").append(v.overtimeSalary))
			.append($("<td></td>").append(v.bonusSalary))
			.append($("<td></td>").append(v.baseSalary+v.lateSalary+v.absenceSalary+v.wuxianyijin+v.overtimeSalary+v.bonusSalary))
			.append($("<td></td>").append(v.salaryTime))
			.append($("<td></td>").append(analysisBtns(v)))
			.appendTo(tbody);
		});
	}, 'json');
	// 获取分页信息
	$.post('./mgr/salary/findSalaryPage', {
		serType : serType,
		serVal : serVal,
		department : department,
		position : position,
		page : page
	}, function(data) {
		$.analysisPage(data.body);}, 'json');
	
}
/*
 * 获取部门列表
 * te5l.com [K]
 */
function findDepartment(){
	$.getJSON('./mgr/findDepartment', function(data){
		var departmentList = $('select.select-department').empty().append("<option value=0>请选择部门</option>");
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i,v){
			$('<option value='+v.id+'></option>').append(v.name).appendTo(departmentList);
		});
	});
}
/*
 * 根据部门ID, 获取职位列表
 * te5l.com [K]
 */
function findPosition(deptId){
	var positionList = $('select.select-position').empty().append("<option value=0>请选择职位</option>");
	if(deptId < 1) return;
	$.getJSON('./mgr/findPositionByDeptId', {deptId : deptId}, function(data){
		if(!$.isSuccess(data)) return;
		$.each(data.body, function(i, v){
			$('<option value='+v.id+'></option>').append(v.name).appendTo(positionList);
		});
	});
}
/*
 * 解析按钮组
 * te5l.com [K]
 */
function analysisBtns(v){
	var btns = "";
	if(v.salaryTime == null || v.salaryTime == ""){
		btns += secure.modify ? "<button type='button' class='btn btn-primary btn-xs' onclick='showSalaryAddBox("+v.emId+")'><span class='glyphicon glyphicon-pencil'></span>录入</button>" : "" ;
	}else{
		btns += secure.find ? "<button type='button' class='btn btn-success btn-xs' onclick='showSalaryModifyBox("+v.id+")'><span class='glyphicon glyphicon-align-left'></span>编辑</button>" : "" ;
	}
	
	btns += secure.modify ? "<button type='button' class='btn btn-info btn-xs' onclick='testexportExcel("+v.id+")'><span class='glyphicon glyphicon-link'></span>导出</button>" : "" ;
	return btns;
}


/**
 * /*
 * 直接导出到E：/中
 * @param id
 */
function exportExcel(id){
	if(!id) return;
	BootstrapDialog.confirm("请确定是否导出工资单?", function(result){
		if(!result) return;
		dialog = BootstrapDialog.isSubmitted();
		$.getJSON('mgr/salary/exportExcelSalaryById', {id:id}, function(data){
			dialog.close();
			if(!$.isSuccess(data)) return;
			BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
			findListInfo();
		});
	});
}
/*
 * 浏览器下载导出通过id查找的员工工资单
 */
function testexportExcel(id){
	if(!id) return;
	BootstrapDialog.confirm("请确定是否导出工资单?", function(result){
		if(!result) return;
		var url="mgr/salary/testexportExcelSalaryById?id="+id;
        window.open(url);

	});
}
/*
 * 浏览器下载导出所有员工工资单
 */
function exportAllExcel(){
	var salaryMonth = $('input.select-salary-time').val();
	BootstrapDialog.confirm("请确定是否导出当月所有员工工资单?", function(result){ 
		if(!result) return;
		var url="mgr/salary/exportAllExcelSalary?salaryMonth=" + salaryMonth;
        window.open(url);

	});
}



/*
 * 显示编辑员工薪酬窗口
 */
function showSalaryModifyBox(id){
	$('.empty').removeClass('empty');
	if(!id) return;
	
	salary.id = id;
	dialog = BootstrapDialog.loading();
	$.getJSON('mgr/salary/findSalaryById', {id:id}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		$('input.modifyBaseSalary').val(data.body.baseSalary);
		$('input.modifyLateSalary').val(data.body.lateSalary);
		$('input.modifyAbsenceSalary').val(data.body.absenceSalary);
		$('input.modifyWuxianyijin').val(data.body.wuxianyijin);
		$('input.modifyOvertimeSalary').val(data.body.overtimeSalary);
		$('input.modifyBonusSalary').val(data.body.bonusSalary);
		$('input.modifyEmId').val(data.body.emId);
		$('input.modifyid').val(data.body.salaryId);
		
		
		BootstrapDialog.showModel($('div.modify-box'));
	});
}

/*
 * 编辑员工薪酬
 * zou
 */
var salary = {};
function modifySalary(){
	
	if(!salary.id) return;
	
	var emId = $.verifyForm($('input.modifyEmId'), true);
	if(!emId) return;
	$.isSubmit = true;
	salary.emId = emId;
	salary.baseSalary = $.verifyForm($('input.modifyBaseSalary'), true);
	salary.lateSalary = $.verifyForm($('input.modifyLateSalary'), true);
	salary.absenceSalary = $.verifyForm($('input.modifyAbsenceSalary'), true);
	salary.wuxianyijin = $.verifyForm($('input.modifyWuxianyijin'), true);
	salary.overtimeSalary = $.verifyForm($('input.modifyOvertimeSalary'), true);
	salary.bonusSalary = $.verifyForm($('input.modifyBonusSalary'), true);
	
	var reg= /^-?\d+(.\d{1,2})?$/;
	
	if(!reg.test(salary.baseSalary) || !reg.test(salary.lateSalary) || !reg.test(salary.absenceSalary) || !reg.test(salary.wuxianyijin) || 
			!reg.test(salary.overtimeSalary) || !reg.test(salary.bonusSalary)){
		BootstrapDialog.msg("输入工资必须为数字，最多有两位小数！！", BootstrapDialog.TYPE_DANGER);
		return;
	}
	
	
	
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/salary/modifySalary', {
		id : salary.id,
		emId : emId,
		baseSalary : salary.baseSalary,
		lateSalary : salary.lateSalary,
		absenceSalary : salary.absenceSalary,
		wuxianyijin : salary.wuxianyijin,
		overtimeSalary : salary.overtimeSalary,
		bonusSalary : salary.bonusSalary,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.modify-box'));
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
		findListInfo();
		
	}, 'json');
}


/*
 * 显示添加薪酬窗口
 * zou
 */
function showSalaryAddBox(emId){
	$('.empty').removeClass('empty');
	$('input.baseSalary').val("");
	$('input.lateSalary').val("");
	$('input.absenceSalary').val("");
	$('input.wuxianyijin').val("");
	$('input.overtimeSalary').val("");
	$('input.bonusSalary').val("");
	$('input.emId').val(emId);
	BootstrapDialog.showModel($('div.add-box'));
}


/*
 * 添加员工薪酬信息
 * zou
 */
var salary = {};
function addSalary(){
	var emId = $.verifyForm($('input.emId'), true);
	if(!emId) return;
	
	
	salary.emId = emId;
	salary.baseSalary = $.verifyForm($('input.baseSalary'), true);
	salary.lateSalary = $.verifyForm($('input.lateSalary'), true);
	salary.absenceSalary = $.verifyForm($('input.absenceSalary'), true);
	salary.wuxianyijin = $.verifyForm($('input.wuxianyijin'), true);
	salary.overtimeSalary = $.verifyForm($('input.overtimeSalary'), true);
	salary.bonusSalary = $.verifyForm($('input.bonusSalary'), true);
	
	var reg= /^-?\d+(.\d{1,2})?$/;
	
	if(!reg.test(salary.baseSalary) || !reg.test(salary.lateSalary) || !reg.test(salary.absenceSalary) || !reg.test(salary.wuxianyijin) || 
			!reg.test(salary.overtimeSalary) || !reg.test(salary.bonusSalary)){
		BootstrapDialog.msg("输入工资必须为数字，最多有两位小数！！", BootstrapDialog.TYPE_DANGER);
		return;
	}
	
	
	/*if(!$.isSubmit) return;*/
	dialog = BootstrapDialog.isSubmitted();
	$.post('mgr/salary/addSalary', {
		emId : emId,
		baseSalary : salary.baseSalary,
		lateSalary : salary.lateSalary,
		absenceSalary : salary.absenceSalary,
		wuxianyijin : salary.wuxianyijin,
		overtimeSalary : salary.overtimeSalary,
		bonusSalary : salary.bonusSalary,
	}, function(data){
		dialog.close();
		if(!$.isSuccess(data)) return;
		BootstrapDialog.hideModel($('div.add-box'));
		findListInfo();
		BootstrapDialog.msg(data.body, BootstrapDialog.TYPE_SUCCESS);
	}, 'json');
}

function selectMonth() {
	WdatePicker({
		dateFmt : 'yyyy-MM',
		isShowToday : false,
		isShowClear : false
	});
} 

package com.empl.mgr.service;

import javax.servlet.http.HttpServletResponse;

import com.empl.mgr.support.JSONReturn;

public interface SalaryService {
	

	public abstract JSONReturn findAllSalaryList(int type, String value, long dept, long position, int page, String salaryMonth, String acctName);
	public abstract JSONReturn findSalaryPage(int type, String value, long dept, long position, int page, String acctName);
	public abstract JSONReturn addSalary(long emId, String baseSalary, String lateSalary, String absenceSalary,
			String wuxianyijin, String overtimeSalary, String bonusSalary, String acctName);
	public abstract JSONReturn modifySalarySalary(long id, long emId, String baseSalary, String lateSalary,
			String absenceSalary, String wuxianyijin, String overtimeSalary, String bonusSalary, String acctName);
	public abstract JSONReturn findSalaryById(long id);
	public abstract JSONReturn exportExcelSalaryById(long id);
	public abstract void testexportExcelSalaryById(long id, HttpServletResponse response);
	public abstract void exportAllExcelSalary(HttpServletResponse response, String salaryMonth);
	
}

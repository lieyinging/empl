package com.empl.mgr.service;

import javax.servlet.http.HttpServletResponse;

import com.empl.mgr.support.JSONReturn;

public interface LeaveService {

	JSONReturn findAllLeaveList(int page);

	JSONReturn findLeavePage(int page);

	JSONReturn addLeave(long emId, String startDate, String endDate, String leaveReason, String acctName);

	JSONReturn findLeaveById(long id);

	JSONReturn verifyLeave(long id, boolean checkResult, String leaveReason, String acctName);

	JSONReturn submitLeave(long id, String acctName);

	JSONReturn updateLeave(long updateLeaveId, String startDate, String endDate, String leaveReason, String acctName);

	JSONReturn findAllMyLeaveList(int page, String acctName);

	JSONReturn findMyLeavePage(int page, String acctName);

	JSONReturn deleteLeave(long id, String acctName);
	

	
	
}

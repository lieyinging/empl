package com.empl.mgr.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.empl.mgr.constant.PageConstant;
import com.empl.mgr.dao.support.AbstractDao;
import com.empl.mgr.model.TeSalary;

@Repository
public class SalaryDao extends AbstractDao<TeSalary> {

	@Override
	public Class<TeSalary> getEntityClass() {
		// TODO Auto-generated method stub
		return TeSalary.class;
	}
	/**
	 * 获取全部已录入员工薪酬列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TeSalary> findSalaryList(String salaryMonth) {
		StringBuffer query = new StringBuffer();
		query.append("from TeSalary where 1=1");
		query.append("and salaryTime = '" + salaryMonth + "'");
		return findSession().createQuery(query.toString()).list();
	}
	/**
	 * 获取页码
	 * @param type
	 * @param value
	 * @param dept
	 * @param position
	 * @param emplType
	 * @return
	 */
	public int findSalaryPage(int type, String value, long dept, long position) {
		/*// TODO Auto-generated method stub
		StringBuffer query = new StringBuffer();
		query.append("select count(*) from TeSalary ts , TeEmployeesBasic empl where ts.employeesId = empl.emId");
		query.append(dept > 0 ? " and empl.emDeparemtn =  " + dept : "");
		query.append(position > 0 ? " and empl.emPosition = " + position : "");
		query.append(type == 1 && StringUtils.isNotEmpty(value) ? " and empl.emFullName like '%" + value + "%' " : "");
		query.append(type == 2 && StringUtils.isNotEmpty(value) ? " and empl.emIdentity like '%" + value + "%' " : "");
		return Integer.parseInt(findSession().createQuery(query.toString()).uniqueResult().toString());*/
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) from TeEmployeesBasic empl where 1=1");
		query.append(dept > 0 ? " and empl.emDeparemtn =  " + dept : "");
		query.append(position > 0 ? " and empl.emPosition = " + position : "");
		query.append(type == 1 && StringUtils.isNotEmpty(value) ? " and empl.emFullName like '%" + value + "%' " : "");
		query.append(type == 2 && StringUtils.isNotEmpty(value) ? " and empl.emIdentity like '%" + value + "%' " : "");
		return Integer.parseInt(findSession().createQuery(query.toString()).uniqueResult().toString());
		
		
	}
	
	
}

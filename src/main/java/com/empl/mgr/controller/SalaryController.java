package com.empl.mgr.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.empl.mgr.annotation.SecureValid;
import com.empl.mgr.constant.MethodType;
import com.empl.mgr.controller.support.AbstractController;
import com.empl.mgr.service.SalaryService;
import com.empl.mgr.support.JSONReturn;

@Scope
@Controller
@RequestMapping(value = "salary")
public class SalaryController extends AbstractController {

	@Autowired
	private SalaryService salaryService;

	
	/**
	 * 获取薪酬列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findAllSalaryList")
	@SecureValid(code = "02002", desc = "获取员工薪酬列表信息", type = MethodType.FIND)
	public JSONReturn findALLSalaryList(@RequestParam int serType, @RequestParam String serVal,
			@RequestParam long department, @RequestParam long position, @RequestParam int page, @RequestParam String salaryMonth, HttpSession httpSession) {
		return salaryService.findAllSalaryList(serType, serVal, department, position, page, salaryMonth, acctName(httpSession));
	}
	/**
	 * 获取薪酬分页列表
	 * @param serType
	 * @param serVal
	 * @param department
	 * @param position
	 * @param page
	 * @param httpSession
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findSalaryPage")
	@SecureValid(code = "02002", desc = "获取员工薪酬分页", type = MethodType.FIND)
	public JSONReturn findSalaryPage(@RequestParam int serType, @RequestParam String serVal,
			@RequestParam long department, @RequestParam long position, @RequestParam int page, HttpSession httpSession) {
		return salaryService.findSalaryPage(serType, serVal, department, position, page, acctName(httpSession));
	}

	/**
	 * 添加员工薪酬
	 * @param user
	 * @param nick
	 * @param pass
	 * @param httpSession
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "addSalary")
	@SecureValid(code = "02002", desc = "添加员工薪酬", type = MethodType.ADD)
	public JSONReturn addAccount(@RequestParam long emId , @RequestParam String baseSalary, @RequestParam String lateSalary, @RequestParam String absenceSalary,
			@RequestParam String wuxianyijin, @RequestParam String overtimeSalary, @RequestParam String bonusSalary,HttpSession httpSession) {
		return salaryService.addSalary(emId, baseSalary, lateSalary, absenceSalary, wuxianyijin, overtimeSalary, bonusSalary, acctName(httpSession));
	}
	
	/**
	 * 修改员工薪酬
	 * @param user
	 * @param nick
	 * @param pass
	 * @param httpSession
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "modifySalary")
	@SecureValid(code = "02002", desc = "修改员工薪酬", type = MethodType.ADD)
	public JSONReturn modifySalaryAccount(@RequestParam long id ,@RequestParam long emId , @RequestParam String baseSalary, @RequestParam String lateSalary, @RequestParam String absenceSalary,
			@RequestParam String wuxianyijin, @RequestParam String overtimeSalary, @RequestParam String bonusSalary,HttpSession httpSession) {
		return salaryService.modifySalarySalary(id, emId, baseSalary, lateSalary, absenceSalary, wuxianyijin, overtimeSalary, bonusSalary, acctName(httpSession));
	}
	
	@ResponseBody
	@RequestMapping(value = "findSalaryById")
	@SecureValid(code = "02002", desc = "根据id获取员工薪酬", type = MethodType.FIND)
	public JSONReturn findSalaryById(@RequestParam long id) {
		return salaryService.findSalaryById(id);
	}
	/**
	 * 导出到E盘中
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "exportExcelSalaryById")
	@SecureValid(code = "02002", desc = "根据id导出员工薪酬", type = MethodType.FIND)
	public JSONReturn exportExcelSalaryById(@RequestParam long id) {
		return salaryService.exportExcelSalaryById(id);
	}
	
	/**
	 * 通过浏览器下载导出当个员工工资单
	 * @param response
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping(value = "testexportExcelSalaryById")
	@SecureValid(code = "02002", desc = "根据id导出员工薪酬")
	public void testexportExcelSalaryById(HttpServletResponse response, @RequestParam long id)throws Exception {
		salaryService.testexportExcelSalaryById(id, response);

	}
	
	/**
	 * 通过浏览器下载导出所有员工工资单
	 * @param response
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping(value = "exportAllExcelSalary")
	@SecureValid(code = "02002", desc = "导出当月所有员工薪酬")
	public void exportAllExcelSalary(HttpServletResponse response, @RequestParam String salaryMonth)throws Exception {
		salaryService.exportAllExcelSalary(response, salaryMonth);

	}

}

package com.empl.mgr.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.empl.mgr.annotation.SecureValid;
import com.empl.mgr.constant.MethodType;
import com.empl.mgr.controller.support.AbstractController;
import com.empl.mgr.dao.LeaveDao;
import com.empl.mgr.service.LeaveService;
import com.empl.mgr.support.JSONReturn;

@Scope
@Controller
@RequestMapping(value = "leave")
public class LeaveController extends AbstractController {

	@Autowired
	private LeaveService leaveService;

	
	/**
	 * 获取所有请假列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findAllLeaveList")
	@SecureValid(code = "05001", desc = "获取员工请假列表信息", type = MethodType.FIND)
	public JSONReturn findAllLeaveList(@RequestParam int page, HttpSession httpSession) {
		return leaveService.findAllLeaveList(page);
	}
	

	/**
	 * 获取我的所有请假列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findAllMyLeaveList")
	@SecureValid(code = "05001", desc = "获取员工请假列表信息", type = MethodType.FIND)
	public JSONReturn findAllMyLeaveList(@RequestParam int page, HttpSession httpSession) {
		return leaveService.findAllMyLeaveList(page, acctName(httpSession));
	}
	
	/**
	 * 获取我的任务分页列表
	 * @param serType
	 * @param serVal
	 * @param department
	 * @param position
	 * @param page
	 * @param httpSession
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findMyLeavePage")
	@SecureValid(code = "05001", desc = "获取员工请假分页", type = MethodType.FIND)
	public JSONReturn findMyLeavePage( @RequestParam int page, HttpSession httpSession) {
		return leaveService.findMyLeavePage(page, acctName(httpSession));
	}
	
	/**
	 * 获取薪酬分页列表
	 * @param serType
	 * @param serVal
	 * @param department
	 * @param position
	 * @param page
	 * @param httpSession
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "findLeavePage")
	@SecureValid(code = "05001", desc = "获取员工请假分页", type = MethodType.FIND)
	public JSONReturn findLeavePage( @RequestParam int page, HttpSession httpSession) {
		return leaveService.findLeavePage(page);
	}

	@ResponseBody
	@RequestMapping(value = "addLeave")
	@SecureValid(code = "05001", desc = "添加员工请假", type = MethodType.ADD)
	public JSONReturn addLeave(@RequestParam long emId , @RequestParam String startDate, @RequestParam String endDate, @RequestParam String leaveReason,
			HttpSession httpSession) {
		return leaveService.addLeave(emId, startDate, endDate, leaveReason, acctName(httpSession));
	}
	
	@ResponseBody
	@RequestMapping(value = "findLeaveById")
	@SecureValid(code = "05001", desc = "根据id获取员工请假单", type = MethodType.FIND)
	public JSONReturn findLeaveById(@RequestParam long id) {
		return leaveService.findLeaveById(id);
	}
	
	@ResponseBody
	@RequestMapping(value = "verifyLeave")
	@SecureValid(code = "05001", desc = "审批员工请假", type = MethodType.MODIFY)
	public JSONReturn verifyLeave(@RequestParam long id , @RequestParam boolean checkResult, @RequestParam String checkReason, 
			HttpSession httpSession) {
		return leaveService.verifyLeave(id, checkResult, checkReason, acctName(httpSession));
	}
	
	@ResponseBody
	@RequestMapping(value = "submitLeave")
	@SecureValid(code = "05001", desc = "提交员工请假", type = MethodType.ADD)
	public JSONReturn submitLeave(@RequestParam long id,  HttpSession httpSession) {
		return leaveService.submitLeave(id, acctName(httpSession));
	}
	
	@ResponseBody
	@RequestMapping(value = "deleteLeave")
	@SecureValid(code = "05001", desc = "提交员工请假")
	public JSONReturn deleteLeave(@RequestParam long id,  HttpSession httpSession) {
		return leaveService.deleteLeave(id, acctName(httpSession));
	}
	
	
	@ResponseBody
	@RequestMapping(value = "updateLeave")
	@SecureValid(code = "05001", desc = "修改员工请假", type = MethodType.ADD)
	public JSONReturn updateLeave(@RequestParam long updateLeaveId , @RequestParam String updateStartDate, @RequestParam String updateEndDate, @RequestParam String updateleaveReason,
			HttpSession httpSession) {
		return leaveService.updateLeave(updateLeaveId, updateStartDate, updateEndDate, updateleaveReason, acctName(httpSession));
	}
}

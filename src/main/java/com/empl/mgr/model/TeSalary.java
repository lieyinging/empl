package com.empl.mgr.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * 员工薪水类
 */
@Entity
@Table(name = "te_salary")
public class TeSalary implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private long salaryId;
	private long employeesId;
	private Date timestamp;
	private double baseSalary;
	private double lateSalary;
	private double absenceSalary;
	private double wuxianyijin;
	private double overtimeSalary;
	private double bonusSalary;
	private String salaryTime;
	

	// Constructors

	/** default constructor */
	public TeSalary() {
	}

	/** full constructor */
	public TeSalary(long salaryId, long employeesId, Date timestamp, double baseSalary, double lateSalary,
			double absenceSalary, double wuxianyijin, double overtimeSalary, double bonusSalary, String salaryTime) {
		super();
		this.salaryId = salaryId;
		this.employeesId = employeesId;
		this.timestamp = timestamp;
		this.baseSalary = baseSalary;
		this.lateSalary = lateSalary;
		this.absenceSalary = absenceSalary;
		this.wuxianyijin = wuxianyijin;
		this.overtimeSalary = overtimeSalary;
		this.bonusSalary = bonusSalary;
		this.salaryTime = salaryTime;
	}
	
	
	
	
	
	

	@Override
	public String toString() {
		return "TeSalary [salaryId=" + salaryId + ", employeesId=" + employeesId + ", timestamp=" + timestamp
				+ ", baseSalary=" + baseSalary + ", lateSalary=" + lateSalary + ", absenceSalary=" + absenceSalary
				+ ", wuxianyijin=" + wuxianyijin + ", overtimeSalary=" + overtimeSalary + ", bonusSalary=" + bonusSalary
				+ ", salaryTime=" + salaryTime + "]";
	}

	
	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "salaryId", unique = true, nullable = false)
	public long getSalaryId() {
		return salaryId;
	}

	public void setSalaryId(long salaryId) {
		this.salaryId = salaryId;
	}

	public long getEmployeesId() {
		return employeesId;
	}

	public void setEmployeesId(long employeesId) {
		this.employeesId = employeesId;
	}
	
	@Version
	@Column(name = "timestamp", nullable = false, length = 19)
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Column(name = "baseSalary")
	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}
	
	@Column(name = "lateSalary")
	public double getLateSalary() {
		return lateSalary;
	}
	
	public void setLateSalary(double lateSalary) {
		this.lateSalary = lateSalary;
	}
	@Column(name = "absenceSalary")
	public double getAbsenceSalary() {
		return absenceSalary;
	}

	public void setAbsenceSalary(double absenceSalary) {
		this.absenceSalary = absenceSalary;
	}
	@Column(name = "wuxianyijin")
	public double getWuxianyijin() {
		return wuxianyijin;
	}

	public void setWuxianyijin(double wuxianyijin) {
		this.wuxianyijin = wuxianyijin;
	}
	@Column(name = "overtimeSalary")
	public double getOvertimeSalary() {
		return overtimeSalary;
	}

	public void setOvertimeSalary(double overtimeSalary) {
		this.overtimeSalary = overtimeSalary;
	}
	@Column(name = "bonusSalary")
	public double getBonusSalary() {
		return bonusSalary;
	}

	public void setBonusSalary(double bonusSalary) {
		this.bonusSalary = bonusSalary;
	}
	@Column(name = "salaryTime")
	public String getSalaryTime() {
		return salaryTime;
	}

	public void setSalaryTime(String salaryTime) {
		this.salaryTime = salaryTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
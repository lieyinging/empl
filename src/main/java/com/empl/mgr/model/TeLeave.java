package com.empl.mgr.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * 员工请假类
 */
@Entity
@Table(name = "te_leave")
public class TeLeave implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private long leaveId;
	private long employeesId;
	private String startTime;
	private String endTime;
	private String leaveReason;
	private Date createTime;
	private Date timestamp;
	
	
	// Constructors
	
	public TeLeave() {
	}
	
	public TeLeave(long leaveId, long employeesId, String leaveReason, String startTime, String endTime,
			Date createTime, Date timestamp) {
		super();
		this.leaveId = leaveId;
		this.employeesId = employeesId;
		this.leaveReason = leaveReason;
		this.startTime = startTime;
		this.endTime = endTime;
		this.createTime = createTime;
		this.timestamp = timestamp;
	}

	@Id
	@Column(name = "leaveId", unique = true, nullable = false)
	public long getLeaveId() {
		return leaveId;
	}


	public void setLeaveId(long leaveId) {
		this.leaveId = leaveId;
	}

	@Column(name = "employeesId")
	public long getEmployeesId() {
		return employeesId;
	}


	public void setEmployeesId(long employeesId) {
		this.employeesId = employeesId;
	}

	@Column(name = "leaveReason")
	public String getLeaveReason() {
		return leaveReason;
	}


	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	@Column(name = "startTime")
	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Column(name = "endTime")
	public String getEndTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Column(name = "createTime")
	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Version
	@Column(name = "timestamp", nullable = false, length = 19)
	public Date getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "TeLeave [leaveId=" + leaveId + ", employeesId=" + employeesId + ", leaveReason=" + leaveReason
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", createTime=" + createTime + ", timestamp="
				+ timestamp + "]";
	}
	

}
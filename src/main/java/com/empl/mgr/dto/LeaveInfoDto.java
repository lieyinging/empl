package com.empl.mgr.dto;

import java.io.Serializable;

/**
 * 请假基本信息类
 * @author zou
 * 将数据库里的员工信息封闭到此类中, 可以屏蔽关键信息, 也能避免暴露数据库字段
 */
public class LeaveInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id; // ID号	
	private long emId; // 员工id
	private String employeesName; // 员工姓名
	private String startTime; //开始时间
	private String endTime; //结束时间
	private String leaveReason; //请假原因
	private String leaveCurrentNode; //开始时间
	private boolean flowState; // 流程状态
	private String leaveCreateTime; //创建时间
	
	
	public LeaveInfoDto() {
	}


	public LeaveInfoDto(long id, long emId, String employeesName, String startTime, String endTime, String leaveReason,
			String leaveCurrentNode, boolean flowState, String leaveCreateTime) {
		super();
		this.id = id;
		this.emId = emId;
		this.employeesName = employeesName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.leaveReason = leaveReason;
		this.leaveCurrentNode = leaveCurrentNode;
		this.flowState = flowState;
		this.leaveCreateTime = leaveCreateTime;
	}


	public LeaveInfoDto(long id, String employeesName, String startTime, String endTime, String leaveReason,
			String leaveCurrentNode, boolean flowState, String leaveCreateTime) {
		super();
		this.id = id;
		this.employeesName = employeesName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.leaveReason = leaveReason;
		this.leaveCurrentNode = leaveCurrentNode;
		this.flowState = flowState;
		this.leaveCreateTime = leaveCreateTime;
	}

	
	
	
	

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getEmId() {
		return emId;
	}


	public void setEmId(long emId) {
		this.emId = emId;
	}


	public String getEmployeesName() {
		return employeesName;
	}


	public void setEmployeesName(String employeesName) {
		this.employeesName = employeesName;
	}


	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public String getEndTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public String getLeaveReason() {
		return leaveReason;
	}


	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}


	public String getLeaveCurrentNode() {
		return leaveCurrentNode;
	}


	public void setLeaveCurrentNode(String leaveCurrentNode) {
		this.leaveCurrentNode = leaveCurrentNode;
	}


	public boolean isFlowState() {
		return flowState;
	}


	public void setFlowState(boolean flowState) {
		this.flowState = flowState;
	}


	public String getLeaveCreateTime() {
		return leaveCreateTime;
	}


	public void setLeaveCreateTime(String leaveCreateTime) {
		this.leaveCreateTime = leaveCreateTime;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "LeaveInfoDto [id=" + id + ", emId=" + emId + ", employeesName=" + employeesName + ", startTime="
				+ startTime + ", endTime=" + endTime + ", leaveReason=" + leaveReason + ", leaveCurrentNode="
				+ leaveCurrentNode + ", flowState=" + flowState + ", leaveCreateTime=" + leaveCreateTime + "]";
	}
	
	

}
